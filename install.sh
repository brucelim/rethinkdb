#!/bin/sh

apt-get update && apt-get install -y wget curl gpg gnupg2 software-properties-common apt-transport-https lsb-release ca-certificates gnupg2
wget -qO- https://download.rethinkdb.com/repository/raw/pubkey.gpg | gpg --dearmor -o /usr/share/keyrings/rethinkdb-archive-keyrings.gpg
echo "deb [signed-by=/usr/share/keyrings/rethinkdb-archive-keyrings.gpg] https://download.rethinkdb.com/repository/ubuntu-$(lsb_release -cs) $(lsb_release -cs) main" | tee /etc/apt/sources.list.d/rethinkdb.list
apt-get update && apt-get install -y rethinkdb python3-pip
rm -rf /var/lib/apt/lists/*
pip3 install rethinkdb
