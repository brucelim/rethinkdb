
# rethinkdb

These rethink commands depends on the RethinkDB Python driver.

- rethinkdb export:
- rethinkdb import:
- rethinkdb dump:
- rethinkdb restore:
- rethinkdb index-rebuild:
- rethinkdb repl:

Building a container that supports these commands.

``` 
docker build -f Dockerfile -t rethinkdb .
docker run -p 8080:8080 -p 28015:28015 -p 29015:29015 -P --name rethink-server -d rethinkdb
docker run -v "$PWD:/data" -p 8080:8080 -p 28015:28015 -p 29015:29015 -P --name rethink-server -d rethinkdb
```
