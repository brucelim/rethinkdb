FROM ubuntu:latest

ADD ./install.sh install.sh
RUN ./install.sh

VOLUME ["/data"]
WORKDIR /data

EXPOSE 8080
EXPOSE 28015
EXPOSE 29015

CMD ["rethinkdb", "--bind", "all"]
